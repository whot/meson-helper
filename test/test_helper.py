#!/usr/bin/env python

from dataclasses import dataclass
from pathlib import Path

import logging
import os
import pytest
import subprocess


logger = logging.getLogger(__name__)


@pytest.fixture(scope="session")
def builddir() -> str:
    return "pytest_builddir"


@pytest.fixture(scope="session")
def fdo_ci_concurrent() -> str:
    return "12"


@pytest.fixture
def testenv(builddir, fdo_ci_concurrent) -> dict[str, str]:
    assert (Path.cwd() / "test" / "meson").exists()
    assert (Path.cwd() / "meson-build.sh").exists()

    meson_path = (Path.cwd() / "test").absolute()
    script_path = Path.cwd().absolute()

    return {
        "PATH": f'{meson_path}:{script_path}:{os.environ["PATH"]}',
        "MESON_BUILDDIR": builddir,
        "FDO_CI_CONCURRENT": fdo_ci_concurrent,
    }


@dataclass
class _TestSetup:
    tmp_path: Path
    builddir: str
    env: dict[str, str]
    default_compile_args: list[str]
    default_test_args: list[str]
    default_install_args: list[str]
    default_dist_args: list[str]


@pytest.fixture
def test_setup(tmp_path, testenv, builddir, fdo_ci_concurrent) -> _TestSetup:
    default_compile_args = ["-v", "-C", builddir, f"-j{fdo_ci_concurrent}"]
    default_test_args = ["-C", builddir, "--print-errorlogs"]
    default_dist_args = ["-C", builddir]
    default_install_args = ["--no-rebuild", "-C", builddir]
    return _TestSetup(
        tmp_path=tmp_path,
        builddir=builddir,
        env=testenv,
        default_compile_args=default_compile_args,
        default_test_args=default_test_args,
        default_dist_args=default_dist_args,
        default_install_args=default_install_args,
    )


@dataclass
class MesonInvocation:
    cmd: str
    args: list[str]

    @classmethod
    def from_file(cls, f: Path) -> list["MesonInvocation"]:
        invocations = []
        with open(f) as fd:
            cmd = None
            args = []
            for line in map(lambda l: l.strip(), fd.readlines()):
                args = line.split("||")
                cmd = args.pop(0)
                logger.debug(f"meson {cmd} {' '.join(args)}")
                invocations.append(MesonInvocation(cmd, args))
        return invocations


def run_script(tmpdir: Path, testenv: dict, args: list[str]) -> list[MesonInvocation]:
    subprocess.run(
        ["meson-build.sh"] + args, stdin=None, cwd=tmpdir, check=True, env=testenv
    )

    outfile = tmpdir / "meson.invocation"
    assert outfile.exists()
    return MesonInvocation.from_file(outfile)


def test_default_invocation(test_setup):
    invocations = run_script(test_setup.tmp_path, test_setup.env, [])

    setup = invocations.pop(0)
    assert setup.cmd == "setup"
    assert setup.args[-1] == test_setup.builddir

    configure = invocations.pop(0)
    assert configure.cmd == "configure"
    assert configure.args[-1] == test_setup.builddir

    compile = invocations.pop(0)
    assert compile.cmd == "compile"
    assert compile.args == test_setup.default_compile_args


@pytest.mark.parametrize("meson_args", [None, "-Dfoo", "-Dbar -Dbaz"])
@pytest.mark.parametrize("meson_extra_args", [None, "-Dextra", "-De2 -De3"])
def test_meson_args(test_setup, meson_args, meson_extra_args):
    if meson_args is not None:
        test_setup.env["MESON_ARGS"] = meson_args
    if meson_extra_args is not None:
        test_setup.env["MESON_EXTRA_ARGS"] = meson_extra_args

    invocations = run_script(test_setup.tmp_path, test_setup.env, [])

    expected_args = []
    if meson_args:
        expected_args += meson_args.split(" ")
    if meson_extra_args:
        expected_args += meson_extra_args.split(" ")

    setup = invocations.pop(0)
    assert setup.cmd == "setup"
    assert setup.args == [test_setup.builddir] + expected_args

    # configure doesn't get any args passed through
    configure = invocations.pop(0)
    assert configure.cmd == "configure"
    assert configure.args == [test_setup.builddir]

    # compile doesn't get any args passed through
    compile = invocations.pop(0)
    assert compile.cmd == "compile"
    assert compile.args == test_setup.default_compile_args


@pytest.mark.parametrize("ninja_args", ["none", "env"])
@pytest.mark.parametrize("do_build", ["none", "skip"])
@pytest.mark.parametrize("do_install", ["none", "env", "skip", "run"])
@pytest.mark.parametrize("do_dist", ["none", "env", "skip", "run"])
@pytest.mark.parametrize("do_test", ["none", "env", "skip", "run"])
def test_meson_steps(
    test_setup,
    ninja_args,
    do_build,
    do_install,
    do_dist,
    do_test,
):
    args = []
    if do_build in ["skip"]:
        args.append(f"--{do_build}-build")
    if do_install in ["skip", "run"]:
        args.append(f"--{do_install}-install")
    if do_dist in ["skip", "run"]:
        args.append(f"--{do_dist}-dist")
    if do_test in ["skip", "run"]:
        args.append(f"--{do_test}-test")

    if ninja_args in ["env"]:
        test_setup.env["NINJA_ARGS"] = "--ninjaaaaa"
    if do_install in ["env"]:
        test_setup.env["MESON_INSTALL_ARGS"] = "--do-install"
    if do_dist in ["env"]:
        test_setup.env["MESON_DIST_ARGS"] = "--do-dist"
    if do_test in ["env"]:
        test_setup.env["MESON_TEST_ARGS"] = "--do-test"

    invocations = run_script(test_setup.tmp_path, test_setup.env, args)

    setup = invocations.pop(0)
    assert setup.cmd == "setup"
    configure = invocations.pop(0)
    assert configure.cmd == "configure"

    if do_build in ["skip"]:
        assert "compile" not in [i.cmd for i in invocations]
    if do_install in ["skip"]:
        assert "install" not in [i.cmd for i in invocations]
    if do_dist in ["skip"]:
        assert "dist" not in [i.cmd for i in invocations]
    if do_test in ["skip"]:
        assert "test" not in [i.cmd for i in invocations]

    if do_build in ["none"]:
        compile = invocations.pop(0)
        assert compile.cmd == "compile"
        expected = test_setup.default_compile_args
        if ninja_args in ["env"]:
            expected.append("--ninja-args")
            expected.append(test_setup.env["NINJA_ARGS"])
        assert compile.args == expected

    if do_test in ["env", "run"]:
        test = invocations.pop(0)
        assert test.cmd == "test"
        expected = test_setup.default_test_args
        if do_test in ["env"]:
            expected.append(test_setup.env["MESON_TEST_ARGS"])
        assert test.args == expected

    if do_install in ["env", "run"]:
        install = invocations.pop(0)
        assert install.cmd == "install"
        expected = test_setup.default_install_args
        if do_install in ["env"]:
            expected.append(test_setup.env["MESON_INSTALL_ARGS"])
        assert install.args == expected

    if do_dist in ["env", "run"]:
        dist = invocations.pop(0)
        assert dist.cmd == "dist"
        expected = test_setup.default_dist_args
        if do_dist in ["env"]:
            expected.append(test_setup.env["MESON_DIST_ARGS"])
        assert dist.args == expected
