# meson-build.sh

This is a helper script for projects using meson to build in the GitLab CI.
It does the usual `meson setup`, `meson build` and optionally `meson test`,
configurable via a few environment variables.

```yaml
somejob:
  script:
    - .gitlab-ci/meson-build.sh --run-test
  variables:
     MESON_BUILDDIR: build_dir
     MESON_ARGS: "-Dfoo=bar"

otherjob:
  script:
    - .gitlab-ci/meson-build.sh --skip-build
  variables:
     MESON_BUILDDIR: build_dir
     MESON_TEST_ARGS: "--setup=valgrind"
```

This repository exists so there's one upstream for this script that can
accumulate and sync changes.
